import React, {useState, useEffect} from 'react';
import { ScrollView, Text, View } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';
import { ActivityIndicator, Colors } from 'react-native-paper';

// this.setState({ photoset: response.data.photosets.photoset }));



function AlbumList (){

  const API_KEY='5694af623c610c5c9d30886c51e293cc';
  const USER_ID='188687328@N07';
  const [photoset, setPhotoSet ] = useState(null);
  
  useEffect (()=>{
    const extraer_data = async() =>{
      try{
        const response = await axios.get(`https://api.flickr.com/services/rest/?method=flickr.photosets.getList&api_key=${API_KEY}&user_id=${USER_ID}&format=json&nojsoncallback=1`)
       setPhotoSet(response.data.photosets.photoset)
      }
      catch(err){
        console.log(err); 
      }
  
    }
    extraer_data()
  },[]);
  
  const renderAlbums =() => {
    return photoset.map(album => <AlbumDetail key={album.id} title={album.title._content}  albumId={album.id}  />);}
  
  
  
  if (!photoset) { 
    return (
      <ActivityIndicator animating={true} size={"large"}></ActivityIndicator>
      );
  }
  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        {renderAlbums()}
      </ScrollView>
    </View>
  );
  
  
}

export default AlbumList;
