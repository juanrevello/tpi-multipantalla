import React from "react";
import { Button } from "react-native-paper";
import { View,Text } from "react-native";

 
const PhotoMenu = props => {

  if (props.ordenadoPorNombre){
    return (   
      <View>
          <Button onPress={props.orderByName}>»  Orden por Nombre  «</Button>
          <Button onPress={props.orderByDate}>Orden por fecha</Button>
      </View>
   );
  }
  if (props.ordenadoPorFecha){
    return (   
      <View>
        <View>
          <Button onPress={props.orderByName}>Orden por Nombre</Button>
          <Button onPress={props.orderByDate}>»  Orden por fecha  «</Button>
        </View>
      </View>
   );
  }
  else{
    return (   
      <View>
          <Button onPress={props.orderByName}>Orden por Nombre</Button>
          <Button onPress={props.orderByDate}>Orden por fecha</Button>
      </View>
   );
  }

};

export default PhotoMenu;

