import React, { useState, useEffect, Fragment } from "react";
import { ScrollView, View } from "react-native";
import axios from "axios";
import PhotoDetail from "./PhotoDetail";
import Card from "./Card";
import Button from "./Button";
import { Actions } from "react-native-router-flux";
import { ActivityIndicator, Colors } from "react-native-paper";
import PhotoHeader from "./PhotoHeader";



function PhotoList({ albumId }) {
  const [photos, setPhotos] = useState([]);
  const [banderaName,setBanderaName] =useState(false);
  const [banderaDate,setBanderaDate] = useState(false);

  const API_KEY = "5694af623c610c5c9d30886c51e293cc";
  const USER_ID = "188687328@N07";

  useEffect(() => {
    const extraer_data = async () => {
      try {
        const response = await axios.get(
          `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${API_KEY}&photoset_id=${albumId}&user_id=${USER_ID}&format=json&nojsoncallback=1`
        );
        setPhotos(response.data.photoset.photo);
        console.log(response.data.photoset.photo)
      } catch (err) {
        console.log(err);
      }
    };
    extraer_data();
  }, []);

  const sortPhotosName = () => {
    setBanderaName(true);
    setBanderaDate(false);
    const sortedPhotos = photos.sort((a, b) => {
      if(a.title.toLowerCase() < b.title.toLowerCase()) return -1;
      if(a.title.toLowerCase() > b.title.toLowerCase()) return 1;
      return 0;
    });

    console.log(sortedPhotos);
    setPhotos([...sortedPhotos]);
    
  };

  const sortPhotosDate = () => {
    setBanderaDate(true);
    setBanderaName(false);
    const sortedPhotos = photos.sort((a, b) => {
      if(a.id < b.id) return -1;
      if(a.id > b.id) return 1;
      return 0;
  });
  setPhotos([...sortedPhotos]);
}

  const renderAlbums = () => {
    return photos.map(photo => (
      <Fragment key={photo.id}>
        <PhotoDetail
          title={photo.title}
          imageUrl={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`}
        />

        <Card>
          <View>
            <Button onPress={() => Actions.comment({ photo_id: photo.id })}>
              Comentarios
            </Button>
          </View>
        </Card>

        {/* <Comments photo_id={photo.id}></Comments> */}
      </Fragment>
    ));
  };

  if (!photos) {
    return (
      <View style={{ flex: 1 }}>
        <ActivityIndicator animating={true} size={"large"}></ActivityIndicator>
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <PhotoHeader orderByName={sortPhotosName} orderByDate={sortPhotosDate} ordenadoPorNombre={banderaName} ordenadoPorFecha={banderaDate}/>
      <ScrollView>        
        {renderAlbums()}
      </ScrollView>
    </View>
  );
}

export default PhotoList;
